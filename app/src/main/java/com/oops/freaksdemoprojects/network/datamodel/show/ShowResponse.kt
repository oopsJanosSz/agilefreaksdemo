package com.szjanos.android.network.datamodel.show


import com.google.gson.annotations.SerializedName
import com.oops.freaksdemoprojects.network.datamodel.show.ShowDataModel

data class ShowResponse(
    @SerializedName("page")
    val page: Int,
    @SerializedName("results")
    val results: MutableList<ShowDataModel>,
    @SerializedName("total_pages")
    val totalPages: Int,
    @SerializedName("total_results")
    val totalResults: Int
) {
    fun hasMorePages(): Boolean = page < totalPages - 1

    fun nextPage() = page + 1
}