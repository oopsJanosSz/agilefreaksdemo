package com.oops.freaksdemoprojects.network

import retrofit2.Response

private const val MAGIC_NUMBER = -104
sealed class Resource<out T : Any> {
    data class Success<out T : Any>(val data: T?) : Resource<T>()
    data class Error(val exception: ServerException) : Resource<Nothing>()
    object Loading : Resource<Nothing>()
}


inline fun <reified T : Any> Response<T>.fetchData(): Resource<T> {
    val result = body()
    return when {
        isSuccessful && result != null -> Resource.Success(result)
        else -> Resource.Error(
            ServerException(
                this.errorBody()?.string() ?: this.message(),
                this.code()
            )
        )
    }
}

class ServerException(private val errorMessage: String, private val responseCode: Int = MAGIC_NUMBER) :
    Exception() {
    override val message: String
        get() = "status code: $responseCode  \n message: $errorMessage"
}