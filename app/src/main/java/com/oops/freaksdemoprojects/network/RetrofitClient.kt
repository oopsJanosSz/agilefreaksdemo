package com.oops.freaksdemoprojects.network


import com.oops.freaksdemoprojects.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


object RetrofitClient {

    const val  TMDBBaseUrl =  "https://api.themoviedb.org/3/";
        const val  API_KEY  ="30e83a71ad071222a344325f01977553"

    fun getTMDBClient(baseUrl: String): Retrofit = Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .client(getDefaultOkHttpClient())
        .build()


    private fun getDefaultOkHttpClient(): OkHttpClient =
        if (BuildConfig.DEBUG) OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor().apply {
                setLevel(HttpLoggingInterceptor.Level.BODY)
            })
            .build() else
            OkHttpClient.Builder().build()

}