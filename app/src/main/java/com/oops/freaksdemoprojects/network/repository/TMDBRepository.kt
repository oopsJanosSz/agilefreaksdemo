package com.oops.freaksdemoprojects.network.repository


import com.oops.freaksdemoprojects.SHOW_FIRST_PAGE
import com.oops.freaksdemoprojects.ShowCategory
import com.oops.freaksdemoprojects.network.TMDBService
import com.oops.freaksdemoprojects.network.fetchData
import javax.inject.Inject

class TMDBRepository @Inject constructor(private val service: TMDBService) {

    suspend fun getShowData(
        showCategory: ShowCategory,
        page: Int = SHOW_FIRST_PAGE
    ) = if (showCategory == ShowCategory.Trending) getTrendingShowData( page)
    else service.getShowData(showCategory.type, page).fetchData()

    private suspend fun getTrendingShowData(
            page: Int = SHOW_FIRST_PAGE,
            isWeekFrame: Boolean = true
    ) = service.getTradingShows(
            timeWindow = if (isWeekFrame) "week" else "day",
            page = page
    ).fetchData()

    suspend fun getImageConfigurations() = service.getImageConfigurations()
}