package com.oops.freaksdemoprojects.network

import com.szjanos.android.network.datamodel.images.ImageConfigurationResponse
import com.szjanos.android.network.datamodel.show.ShowResponse

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface TMDBService {

    @GET("tv/{category}?api_key=${RetrofitClient.API_KEY}")
    suspend fun getShowData(
            @Path("category") category: String,
            @Query("page") page: Int
    ): Response<ShowResponse>

    @GET("configuration?api_key=${RetrofitClient.API_KEY}")
    suspend fun getImageConfigurations(): Response<ImageConfigurationResponse>

    @GET("trending/tv/{time_window}?api_key=${RetrofitClient.API_KEY}")
    suspend fun getTradingShows(
            @Path("time_window") timeWindow: String,
            @Query("page") page: Int
    ): Response<ShowResponse>
}