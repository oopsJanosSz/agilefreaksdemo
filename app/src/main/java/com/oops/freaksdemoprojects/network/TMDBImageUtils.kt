package com.oops.freaksdemoprojects.network

import android.net.Uri
import com.oops.freaksdemoprojects.PersistenceHolder


object TMDBImageUtils {


    private val imageConfig by lazy {
        PersistenceHolder.imageConfig
    }

    fun getPosterUrlForMovie(imagePath: String, imageType: ImageType): String {
        return buildImageUrl(imageType, imagePath)
    }

    private fun buildImageUrl(imageType: ImageType, imagePath: String): String =
        Uri.parse(imageConfig.secureBaseUrl).buildUpon()
            .appendEncodedPath(imageType.size)
            .appendEncodedPath(imagePath).build()
            .toString()

}

enum class ImageType(val size: String) {
    Poster("w500"),
    BackDrop("w1280")
}