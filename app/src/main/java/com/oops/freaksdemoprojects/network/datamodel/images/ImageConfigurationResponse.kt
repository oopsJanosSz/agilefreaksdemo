package com.szjanos.android.network.datamodel.images
import com.google.gson.annotations.SerializedName

data class ImageConfigurationResponse(
    @SerializedName("images")
    val imageConfiguration: ImageConfiguration
)