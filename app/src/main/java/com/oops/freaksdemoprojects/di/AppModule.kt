package com.oops.freaksdemoprojects.di

import android.content.Context
import com.oops.freaksdemoprojects.database.AppDatabase
import com.oops.freaksdemoprojects.database.RoomDatabsae
import com.oops.freaksdemoprojects.network.RetrofitClient
import com.oops.freaksdemoprojects.network.TMDBService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideTMDBService(): TMDBService = RetrofitClient.getTMDBClient(RetrofitClient.TMDBBaseUrl).create(
        TMDBService::class.java)


    @Provides
    @Singleton
    fun provideAppDatabase(@ApplicationContext context: Context): AppDatabase {
        return RoomDatabsae(context).database
    }
}