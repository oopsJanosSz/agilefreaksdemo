package com.oops.freaksdemoprojects.screens.splashscreen

import android.animation.Animator
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.airbnb.lottie.LottieAnimationView
import com.google.android.material.snackbar.Snackbar
import com.oops.freaksdemoprojects.R
import com.oops.freaksdemoprojects.network.Resource

import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SplashScreenFragment : Fragment() {

    private val splashScreenViewModel: SplashScreenViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_splash_screen, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val lottieSplashScreenAnimationView =
            view.rootView.findViewById<LottieAnimationView>(R.id.lottie_loading_animation)

        splashScreenViewModel.configurationLiveData.observe(viewLifecycleOwner, Observer { res: Resource<Any> ->
            when (res) {
                is Resource.Loading -> lottieSplashScreenAnimationView.playAnimation()
                is Resource.Success -> findNavController().navigate(R.id.action_splashScreenFragment_to_mainFragment)
                is Resource.Error -> view.snack(res.exception.message)
            }
        })

        splashScreenViewModel.loadData()
    }

    private fun View.snack(message: String, duration: Int = Snackbar.LENGTH_LONG) {
        Snackbar.make(this, message, duration).show()
    }
}