package com.oops.freaksdemoprojects.screens.mainscreen


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.google.android.material.tabs.TabLayoutMediator
import com.oops.freaksdemoprojects.R
import com.oops.freaksdemoprojects.databinding.MainFragmentBinding
import com.oops.freaksdemoprojects.screens.mainscreen.viewpager.PageAdapter

class MainFragment : Fragment() {

    private lateinit var binding: MainFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = DataBindingUtil.inflate<MainFragmentBinding>(
        inflater,
        R.layout.main_fragment,
        container,
        false
    ).apply {
        lifecycleOwner = viewLifecycleOwner
    }.also {
        binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val adapter = PageAdapter(this)
        with(binding) {
            mainScreenPager.adapter = adapter
            tabLayout.apply {
                TabLayoutMediator(this, mainScreenPager) { tab, position ->
                    tab.text = adapter.getTabName(position)
                }.attach()
            }
        }
    }
}
