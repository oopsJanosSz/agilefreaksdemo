package com.oops.freaksdemoprojects.screens.mainscreen.viewpager

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.oops.freaksdemoprojects.ShowCategory
import java.util.*

class PageAdapter(fragment: Fragment) : FragmentStateAdapter(fragment) {

    private val showCategories = ShowCategory.values()

    override fun getItemCount(): Int = showCategories.size

    override fun createFragment(position: Int): Fragment =
        PageFragment().apply {
            arguments = Bundle().apply {
                putSerializable(
                    PagerConstants.PAGER_SHOW_CATEGORY_KEY,
                    showCategories[position])
            }
        }

    fun getTabName(position: Int) = showCategories[position].name

}