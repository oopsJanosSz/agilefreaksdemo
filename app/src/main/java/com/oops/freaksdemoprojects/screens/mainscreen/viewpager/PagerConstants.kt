package com.oops.freaksdemoprojects.screens.mainscreen.viewpager


object PagerConstants {
    const val PAGER_SHOW_CATEGORY_KEY = "show-category-key"
    const val PAGER_SHOW_TYPE ="show-type-key"
}