package com.oops.freaksdemoprojects.screens

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.oops.freaksdemoprojects.R
import com.oops.freaksdemoprojects.databinding.ItemOnboardingPageRecyclerBinding
import com.oops.freaksdemoprojects.network.ImageType
import com.oops.freaksdemoprojects.network.TMDBImageUtils

import com.oops.freaksdemoprojects.network.datamodel.show.ShowDataModel

class ShowsAdapter : RecyclerView.Adapter<ShowsAdapter.ShowHolder>() {

    private val items = mutableListOf<ShowDataModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShowHolder {
        val binding = DataBindingUtil.inflate<ItemOnboardingPageRecyclerBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_onboarding_page_recycler,
            parent,
            false
        )
        return ShowHolder(binding)
    }

    fun setItems(newItems: List<ShowDataModel>) {
        items.addAll(newItems)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: ShowHolder, position: Int) = holder.bind(items[position])

    override fun getItemCount() = items.size

    inner class ShowHolder(private val binding: ItemOnboardingPageRecyclerBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: ShowDataModel) = with(binding) {
            imageUrl = TMDBImageUtils.getPosterUrlForMovie(item.posterPath, ImageType.Poster)
            binding.showPosterImageView.setOnClickListener {
                Toast.makeText(it.context,"Yey, This is only for demo.... btw.. You clicked on ${item.name} ",Toast.LENGTH_SHORT).show()
            }
            executePendingBindings()
        }
    }
}