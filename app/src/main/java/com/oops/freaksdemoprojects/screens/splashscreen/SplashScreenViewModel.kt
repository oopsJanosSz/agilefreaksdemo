package com.oops.freaksdemoprojects.screens.splashscreen


import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.oops.freaksdemoprojects.PersistenceHolder
import com.oops.freaksdemoprojects.network.repository.TMDBRepository

import com.oops.freaksdemoprojects.network.Resource
import com.oops.freaksdemoprojects.network.ServerException
import com.szjanos.android.network.datamodel.images.ImageConfiguration

import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject


@HiltViewModel
class SplashScreenViewModel @Inject constructor(
        private val tmdbRepository: TMDBRepository
) : ViewModel() {

    val configurationLiveData = MutableLiveData<Resource<ImageConfiguration>>()

   private suspend fun getImageConfiguration() {
        val response = tmdbRepository.getImageConfigurations()
        when {
            response.isSuccessful -> {
                val imageConfig = response.body()?.imageConfiguration
                imageConfig?.let { PersistenceHolder.imageConfig = it }
                configurationLiveData.postValue(Resource.Success(imageConfig))
            }
            else -> {
                val message = response.message()
                configurationLiveData.postValue(Resource.Error(ServerException(message)))
            }
        }
    }

    fun loadData() = viewModelScope.launch {
        getImageConfiguration()
    }

}