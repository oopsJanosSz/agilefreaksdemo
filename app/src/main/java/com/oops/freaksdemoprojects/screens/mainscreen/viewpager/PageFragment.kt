package com.oops.freaksdemoprojects.screens.mainscreen.viewpager

import android.graphics.Rect
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.oops.freaksdemoprojects.R
import com.oops.freaksdemoprojects.ShowCategory

import com.oops.freaksdemoprojects.databinding.OnboardingPageFragmentBinding

import com.oops.freaksdemoprojects.screens.ShowsAdapter

import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class PageFragment : Fragment(){

    private val viewModel by viewModels<PageViewModel>()
    private lateinit var binding: OnboardingPageFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = DataBindingUtil.inflate<OnboardingPageFragmentBinding>(
        inflater,
        R.layout.onboarding_page_fragment,
        container,
        false
    ).apply {
        lifecycleOwner = viewLifecycleOwner
    }.also {
        binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(binding) {
            onboardingPageRecyclerView.adapter = ShowsAdapter()
            val spanCount = (onboardingPageRecyclerView.layoutManager as GridLayoutManager).spanCount
            onboardingPageRecyclerView.addItemDecoration(SpacingItemDecorator(spanCount))
            viewModel = this@PageFragment.viewModel
        }.also {
            val showCategory = (arguments?.getSerializable(PagerConstants.PAGER_SHOW_CATEGORY_KEY)
                ?: ShowCategory.Popular) as ShowCategory
            viewModel.getShows(showCategory)
        }
    }

    private class SpacingItemDecorator(val spanCount: Int) : RecyclerView.ItemDecoration() {
        override fun getItemOffsets(
            outRect: Rect,
            view: View,
            parent: RecyclerView,
            state: RecyclerView.State
        ) {
            super.getItemOffsets(outRect, view, parent, state)
            val position = parent.getChildAdapterPosition(view)
            val column = position % 3;
            val spacing = 50;

            with(outRect){
                left = spacing - column * spacing / spanCount
                right = (column + 1) * spacing / spanCount
                if (position < spanCount) top = spacing
                bottom = spacing
            }

        }
    }
}

