package com.oops.freaksdemoprojects.screens

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.oops.freaksdemoprojects.network.datamodel.show.ShowDataModel

object BindingAdapters {

    @JvmStatic
    @BindingAdapter("items")
    fun setItems(view: RecyclerView, items: List<ShowDataModel>?) = items?.let {
        when (val adapter = view.adapter) {
            is ShowsAdapter -> adapter.setItems(it)
        }
    }

    @JvmStatic
    @BindingAdapter("imageUrl")
    fun loadImage(view: ImageView, imageUrl: String?) = imageUrl?.let {
        Glide.with(view).load(it).into(view)
    }
}