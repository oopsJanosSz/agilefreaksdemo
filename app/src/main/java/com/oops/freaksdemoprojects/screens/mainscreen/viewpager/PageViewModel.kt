package com.oops.freaksdemoprojects.screens.mainscreen.viewpager

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope

import com.oops.freaksdemoprojects.ShowCategory

import com.oops.freaksdemoprojects.network.datamodel.show.ShowDataModel
import com.oops.freaksdemoprojects.network.repository.TMDBRepository

import com.oops.freaksdemoprojects.network.Resource

import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PageViewModel @Inject constructor(
    private val tmdbRepository: TMDBRepository
) : ViewModel() {

    private val _showsLiveData = MutableLiveData<List<ShowDataModel>>()

    val showsData: LiveData<List<ShowDataModel>>
        get() = _showsLiveData

    fun getShows(showCategory: ShowCategory) {
        viewModelScope.launch {
            when (val resource = tmdbRepository.getShowData(showCategory)) {
                is Resource.Success -> _showsLiveData.postValue(resource.data?.results)
                is Resource.Error -> {
                //TODo
                }
            }
        }
    }
}