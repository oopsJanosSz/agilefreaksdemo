package com.oops.freaksdemoprojects

enum class ShowCategory(val type:String){
    Popular("popular"),
    TopRated("top_rated"),
    Trending("trending"),
}

const val SHOW_FIRST_PAGE = 1